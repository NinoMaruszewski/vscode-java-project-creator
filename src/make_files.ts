import * as vscode from 'vscode';
import fs = require('fs');
import child_process = require('child_process');
import os = require('os');

export async function make_files(path: string, git_init: true | false, gradle: true | false) {
    /**
     * Just a do-everything make file fn
     */
    // git stuff
    if (git_init) {
        child_process.exec("git init", (err) => { if (err) { console.error(err); } });
    }

    // here gradle does everything for files
    if (gradle) {
        child_process.exec("gradle init --type java-application", (err) => { if (err) { console.error(err); } });
    }
    // our own function
    else {
        // get pack name
        let pkg_name = await ask_pkg_name();
        // if its empty, quit
        if (!pkg_name) {
            return vscode.window.showWarningMessage('Package name not specified, exiting java project creation');
        } else {
            gen_java_files(path, pkg_name);
        }
    }
};

function gen_java_files(path: string, package_name_arr: Array<string>) {
    /**
     * Makes the java files according to the default schema:
     * project_folder
     * |-src
     * |  |-main
     * |  |  |-java/package_name(com.example.test)/
     * |  |  |-resources/
     * |  |-test
     * |     |-java/package_name/
     * |     |-resources/
     * |-bin
     * |  |-main/package_name/
     * |  |-test/package_name/
     * |-.gitignore
     */
    // windows
    if (os.platform() === 'win32') {
        // paths
        const package_name = package_name_arr.join('\\');
        const main_src = path + '\\src\\main\\java\\' + package_name;
        const main_res = path + '\\src\\main\\resources';
        const test_src = path + '\\src\\test\\java\\' + package_name;
        const test_res = path + '\\src\\test\\resources';
        // make directories
        make_directories([main_src,main_res,test_src, test_res]);
    }
    // *nix
    else {
        const package_name = package_name_arr.join('/');
        const main_src = path + '/src/main/java/' + package_name;
        const main_res = path + '/src/main/resources';
        const test_src = path + '/src/test/java/' + package_name;
        const test_res = path + '/src/test/resources';
        // make directories
        make_directories([main_src,main_res,test_src, test_res]);
    }
}

async function ask_pkg_name() {
    /**
     * Returns an array of a package name, by asking the user for the name:
     * 
     * Example:
     * User -> com.example.test
     * returns ['com', 'example', 'test']
     */
    let pack_name = await vscode.window.showInputBox({
        prompt: "Package Name",
        placeHolder: "com.example.test"
    });
    if (!pack_name) {
        return;
    } else {
        return pack_name.split('.');
    }
}

function make_directories(paths: Array<string>) {
    for (let path of paths){
        fs.mkdirSync(path);
    }
}