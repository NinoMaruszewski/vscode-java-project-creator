// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as make_files from './make_files';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "java-project-creator" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let create_java_project = vscode.commands.registerCommand('java-project-creator.createJavaProject', () => {
		vscode.window.showOpenDialog({
			canSelectFiles: false,
			canSelectFolders: true,
			canSelectMany: false
		}).then((path) => {
			if (!path) {
				vscode.window.showWarningMessage("Please select a folder to create a Java project in.");
			}
			else {
				make_files.make_files(path[0].fsPath, false, false);
			}
		});
	});

	context.subscriptions.push(create_java_project);

	let create_gradle_project = vscode.commands.registerCommand('java-project-creator.createGradleJavaApp', () => {
		vscode.window.showInformationMessage('Create Gradle Java App');
	});
	context.subscriptions.push(create_gradle_project);
}

// this method is called when your extension is deactivated
export function deactivate() {}
