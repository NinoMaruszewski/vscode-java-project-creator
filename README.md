# java-project-creator README

## Features

Describe specific features of your extension including screenshots of your extension in action. Image paths are relative to this README file.

For example if there is an image subfolder under your extension project workspace:

\!\[feature X\]\(images/feature-x.png\)

> Tip: Many popular extensions utilize animations. This is an excellent way to show off your extension! We recommend short, focused animations that are easy to follow.

## Requirements
- [Language Support for Java(TM)](https://marketplace.visualstudio.com/items?itemName=redhat.java) by Red Hat
- VSCode >= 1.46.0

## Extension Settings

Include if your extension adds any VS Code settings through the `contributes.configuration` extension point.

For example:

This extension contributes the following settings:

* `myExtension.enable`: enable/disable this extension
* `myExtension.thing`: set to `blah` to do something

## Known Issues

None so far, but the issue tracker is [here](https://bitbucket.org/NinoMaruszewski/vscode-java-project-creator/issues?status=new&status=open).
## Release Notes

### 1.0.0

Initial release of extension, basic Java project support
